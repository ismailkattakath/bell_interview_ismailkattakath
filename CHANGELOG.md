# Changelog
Version 2.0.0 is a complete rewrite of Price Calculator library for enabling backward compatibility and concurrency.

## [2.0.0] - 2018-07-27
### Added
- Implemented Javascript Prototype for supporting concurrent usage of the library
- Added more Mocha test cases

### Changed
- Arrow functions to regular functions
- `let` and `const` to `var`
- `Promise` to `Callback`

### Removed
- Bluebird
- Chai
- Chai As Promise