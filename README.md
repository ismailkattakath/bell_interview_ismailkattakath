# Price calculator

Price Calculator is a Node.js library for Bell Resellers to calculate the Total Cost of an order based on number of items, unit price and Province/State. The Total Cost of order is after deducting volume discounts and including Provincial/State Sales Tax.

## Version
2.0.0 ([CHANGELOG](https://github.com/shawnbutton/bell_interview_ismailkattakath/blob/price-calculator-v2/CHANGELOG.md))

## Requirements

The Source Code is optimised for running on `node version 0.10.26` or above. However, to install **Price Calculator** using `npm` Package Manager as a dependency on your application, `npm version 5.6.0` or above is required. Hence, it is recommended to use `node version 8.11.3`, which comes with `npm version 5.6.0` in bundle.

## How to use
Follow the instructions to below to use Price Calculator library in your application

### Installation
Run the following command at the root of your application.

```
$ npm install --save 'git+https://<githubusername>:<githubpassword>@github.com/shawnbutton/bell_interview_ismailkattakath.git#price-calculator-v2'
```
> **NOTE**: Replace `<githubusername>` and `<githubpassword>` with the credentials of your GitHub account, which has access to this repository.

### Alternate Method
If the above command do not work or you do not have access to this repository, use the following command instead.

````
$ npm i -s git+https://ismailkattakath@bitbucket.org/ismailkattakath/bell_interview_ismailkattakath.git
````
_(This is a secondary repository hosted on BitBucket)_

> **NOTE**: To install dependencies from Git Repositories using `npm` Package Manager, you need to use **NPM 5** or above.

### Sample Usage

Import Price Calculator to your JS file

```javascript
var PriceCalculator = require ('bell_interview_ismailkattakath');
```
Then create a **Price Calculator** object with input data. Refer [Input Data](#input-data) below for more information.

```javascript
var priceCalc = new PriceCalculator({
    itemCount: 500,
    itemPrice: 1,
    stateCode: 'ON'
});

priceCalc.getTotal(function (err) {
    if (err) console.error(err);
    else console.log('Total Cost: ' + priceCalc.totalCost);
});
```

### Input Data

The following table lists Input Data. They should be passed into `getTotalCost` function in the exact order and as given below. **All of them are mandatory**.

| Order  | Input         | Data Type   | Example    |
|--------|---------------|-------------|------------|
| 1      | `itemCount`   | Number     | *3600*        |
| 2      | `itemPrice`   | Number       | *2.25*      |
| 3      | `stateCode`   | String      | *MI*       |



## Testing

To run test suit, execute following commands on command line

```shell
$ git clone https://github.com/shawnbutton/bell_interview_ismailkattakath.git
$ cd bell_interview_ismailkattakath
$ git checkout price-calculator-v2
$ npm install
$ npm test
```
> **NOTE**: You need to provide the credentials of your GitHub account which has access to this repository when prompted. If the Merge Request to merge `price-calculator-v2` branch with `master` branch has beel fulfilled already, you may avoid the third command above.

## Example

A command line sample application is provided in the [samples](https://github.com/shawnbutton/bell_interview_ismailkattakath/tree/price-calculator-v2/samples) folder. Clone this repository, navigate to `samples` folder and execute the following on command-line to try it:

```shell
$ node command-line.js 3600 2.25 MI
```

The first argument (`3600`) is `itemCount`, second argument (`2.25`) is `itemPrice` and the last one (`MI`) represents the `stateCode` parameter. You can try different arguments to test manually.

## Credits

  - [Ismail Kattakath](http://github.com/ismailkattakath)
  - Mocha
  - Underscore
