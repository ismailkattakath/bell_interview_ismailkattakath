'use strict';

/**
 *
 * Price Calculator
 * @desc Price calculation library for resellers
 * which accept number of items, unit price and 2-letter province/state code as input
 * and outputs total price deducting volume discounts including province/state sales tax.* *
 * @author Ismail Kattakath
 * @version 2.0.0
 *
 */

var underscore = require('underscore');
var fs = require('fs');
var path = require('path');
var discountRatesFilePath = '../data/discount-rates.json';
var taxRatesFilePath = '../data/tax-rates.json';

/**
 *
 * @param {object} input
 * @param {number} input.itemCount Product count (Example: 1)
 * @param {number} input.itemPrice Unit price (Example: 2.34)
 * @param {string} input.stateCode 2 letter State/ Province code (Example: ON)
 *
 * @example
 * var PriceCalculator = require ('bell_interview_ismailkattakath');
 * var priceCalc = new Pricecalculator({
        itemCount: 500,
        itemPrice: 1,
        stateCode: 'ON'
    });
 * priceCalc.getTotal(function (err) {
        if (err) console.error(err);
        else console.log('Total Cost of order: $' + priceCalc.totalCost);
    });
 *
 * @constructor
 */
function PriceCalculator(input) {
    this.priceCalc = underscore.extend({}, {
        itemCount: null,
        itemPrice: null,
        stateCode: null,
        data: {
            taxRates: null,
            discountRates: null
        },
        totalCost: null
    }, input);
}

/**
 * @desc Validate Input and Calculate Total Cost of purchase according to Data
 * @param callback
 */
PriceCalculator.prototype.getTotal = function (callback) {
    var _this = this;

    this.loadData(function (err) {
        if (err) callback(err); else _this.validateInput(function (err) {
            if (err) callback(err); else _this.calculate(function (err) {
                if (err) callback(err); else callback();
            });
        });
    });
};

/**
 * @desc Load Data from JSON data source files
 * @param callback
 */
PriceCalculator.prototype.loadData = function (callback) {
    var _this2 = this;

    /**
     * @desc Process raw Data for numeric operations
     * @param callback
     */
    var processData = function processData(callback) {
        var processedData = {};
        Object.keys(_this2.priceCalc.data).forEach(function (dataKey) {
            processedData[dataKey] = [];
            _this2.priceCalc.data[dataKey].forEach(function (dataObj) {
                Object.keys(dataObj).forEach(function (dataObjKey) {
                    dataObj[dataObjKey] = dataObj[dataObjKey].replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
                    dataObj[dataObjKey] = !isNaN(dataObj[dataObjKey]) ? parseFloat(dataObj[dataObjKey]) : dataObj[dataObjKey];
                });
                processedData[dataKey].push(dataObj);
            });
        });
        _this2.priceCalc.data = processedData;
        callback();
    };

    fs.readFile(path.join(__dirname, taxRatesFilePath), function (err, data) {
        if (err) callback(err); else {
            _this2.priceCalc.data.taxRates = JSON.parse(data);
            fs.readFile(path.join(__dirname, discountRatesFilePath), function (err, data) {
                if (err) callback(err); else {
                    _this2.priceCalc.data.discountRates = JSON.parse(data);
                    processData(callback);
                }
            });
        }
    });
};

/**
 * @desc Validate all the 3 mandatory inputs - itemCount, itemPrice and stateCode
 * @param callback
 */
PriceCalculator.prototype.validateInput = function (callback) {
    var _this3 = this;

    /**
     * @desc Check if Tax Rate of the State/Privince code provided
     * @param stateCode
     * @return {boolean}
     */
    var stateCodeFound = function stateCodeFound(stateCode) {
        var hasFound = false;
        _this3.priceCalc.data.taxRates.forEach(function (taxRate) {
            if (stateCode === taxRate['Province']) hasFound = true;
        });
        return hasFound;
    };

    if (!this.priceCalc.itemCount || typeof this.priceCalc.itemCount !== 'number' || this.priceCalc.itemCount < 1)
        callback('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)');
    else if (!this.priceCalc.itemPrice || typeof this.priceCalc.itemPrice !== 'number' || this.priceCalc.itemPrice < 1)
        callback('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)');
    else if (!this.priceCalc.stateCode || typeof this.priceCalc.stateCode !== 'string' || this.priceCalc.stateCode.length !== 2)
        callback('stateCode is required and it should be a 2 character String (E.g. \'MI\')');
    else if (!stateCodeFound(this.priceCalc.stateCode))
        callback('Tax rate of the State/Province "' + this.priceCalc.stateCode + '" is not found');
    else callback();
};

/**
 * @desc Calculate Total Cost of purchase
 * @param callback
 */
PriceCalculator.prototype.calculate = function (callback) {
    var _this4 = this;

    /**
     * @desc include Sales Tax with total discounted cost
     * @param discountedCost
     * @param callback
     */
    var includeSalesTax = function includeSalesTax(discountedCost, callback) {
        var taxRates = _this4.priceCalc.data.taxRates;
        taxRates.forEach(function (rate) {
            if (rate['Province'] === _this4.priceCalc.stateCode) {
                _this4.totalCost = discountedCost + discountedCost * (rate['Tax Rate'] / 100);
                callback();
            }
        });
    };

    /**
     *
     * @desc Find discounted price based on a discount rate
     * @param value
     * @param discountRate
     * @return {number}
     */
    var deductDiscount = function deductDiscount(value, discountRate) {
        return value - value * (discountRate / 100);
    };

    /**
     * @desc Apply discount based on Discount Rate data
     * @param actualCost
     * @param callback
     */
    var applyDiscount = function applyDiscount(actualCost, callback) {
        var discountRates = underscore.sortBy(_this4.priceCalc.data.discountRates, 'Order Value');
        var discountedCost = actualCost;
        for (var i = 0; i < discountRates.length; i++) {
            if (discountRates[i + 1]) {
                if (actualCost >= discountRates[i]['Order Value'] && actualCost < discountRates[i + 1]['Order Value']) {
                    discountedCost = deductDiscount(actualCost, discountRates[i]['Discount Rate']);
                }
            } else {
                if (actualCost >= discountRates[i]['Order Value']) {
                    discountedCost = deductDiscount(actualCost, discountRates[i]['Discount Rate']);
                }
            }
        }
        callback(parseFloat(discountedCost.toFixed(2)));
    };

    applyDiscount(this.priceCalc.itemCount * this.priceCalc.itemPrice, function (discountedCost) {
        includeSalesTax(discountedCost, callback);
    });
};

/**
 * @export {object}
 * @type {PriceCalculator}
 */
module.exports = PriceCalculator;