'use strict';

/**
 * @usage
 * node command-line.js <itemCount> <itemPrice> <stateCode>
 *
 * @Example
 * node command-line.js 3600 2.25 MI
 */

/**
 * @desc Importing Price Calculator library
 */
var PriceCalculator = require('../lib');

/**
 * @desc Loading command line arguments
 */
var args = [];
process.argv.forEach(function(value) {
    args.push(value)
});

/**
 * @desc Setting up input data from command line arguments or default values
 */
var itemCount = args[2] && !isNaN(args[2]) ? parseInt(args[2]) : 500;
var itemPrice = args[3] && !isNaN(args[3]) ? parseFloat(args[3]): 1;
var stateCode = args[4] ? args[4] : 'ON';

/**
 * @desc Creating Price Calculator object by passing input data to the constructor
 */
var priceCalc = new PriceCalculator({
    itemCount: itemCount,
    itemPrice: itemPrice,
    stateCode: stateCode
});

/**
 * @desc Calculating the Total Cost of order based on the input data
 */
priceCalc.getTotal(function (err) {
    if (err) console.error(err);
    else console.log('Total Cost of order: $' + priceCalc.totalCost);
});