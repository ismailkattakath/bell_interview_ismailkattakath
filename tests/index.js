var assert = require('assert');

var PriceCalculator = require('../lib');

describe('PriceCalculator', function () {
    describe('getTotal function', function () {

        it('Passes with 565.00', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 500,
                itemPrice: 1,
                stateCode: 'ON'
            });
            priceCalc.getTotal(function (err) {
                if (err) done(err);
                else {
                    assert.equal(565.00, priceCalc.totalCost);
                    done()
                }
            });
        });

        it('Passes with 7984.98', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) done(err);
                else {
                    assert.equal(7984.98, priceCalc.totalCost);
                    done()
                }
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: '3600',
                itemPrice: 2.25,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 0,
                itemPrice: 2.25,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: null,
                itemPrice: 2.25,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: undefined,
                itemPrice: 2.25,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: '2.25',
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 0,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: null,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: undefined,
                stateCode: 'MI'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'stateCode is required and it should be a 2 character String (E.g. \'MI\')\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: 0
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('stateCode is required and it should be a 2 character String (E.g. \'MI\')', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'stateCode is required and it should be a 2 character String (E.g. \'MI\')\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: 100
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('stateCode is required and it should be a 2 character String (E.g. \'MI\')', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'stateCode is required and it should be a 2 character String (E.g. \'MI\')\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: null
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('stateCode is required and it should be a 2 character String (E.g. \'MI\')', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'stateCode is required and it should be a 2 character String (E.g. \'MI\')\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: undefined
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('stateCode is required and it should be a 2 character String (E.g. \'MI\')', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'stateCode is required and it should be a 2 character String (E.g. \'MI\')\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: 'Ontario'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('stateCode is required and it should be a 2 character String (E.g. \'MI\')', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'Tax rate of the State/Province "NY" is not found\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600,
                itemPrice: 2.25,
                stateCode: 'NY'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('Tax rate of the State/Province "NY" is not found', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator();
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({});
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemCount: 3600
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemPrice is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                itemPrice: 2.25
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                stateCode: 'NY'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator({
                test: 'OK'
            });
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator(null);
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

        it('Fails with error: \'itemCount is required and it should be a Number greater than 0 (E.g. 1.23)\'', function (done) {
            var priceCalc = new PriceCalculator(undefined);
            priceCalc.getTotal(function (err) {
                if (err) {
                    assert.equal('itemCount is required and it should be a Number greater than 0 (E.g. 1.23)', err);
                    done();
                }
                else done(err)
            });
        });

    });
});